const express     = require('express');
const bodyParser  = require('body-parser');
const app         = express();

// Import methods to handle the routes
const { createCompany, signIn } = require('./routes/companies');
const { verifyToken } = require('./routes/token');
const suppliers = require('./routes/suppliers');
const { uploadImage } = require('./routes/uploadFile');
const incomesAndExpenses = require('./routes/incomes_and_expenses');

// Import the middlewares
const { headers, getUserId } = require('./middlewares')

const port = process.env.PORT || 8000;
const apiRoutes = express.Router();

// Middlewares
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({limit: '2mb', type: 'application/json'}));
app.use(headers);
app.use('/api', getUserId);
app.use('/api', apiRoutes);

// Company
app.post('/company', createCompany);
app.post('/company/signIn', signIn);

// Token
apiRoutes.get('/verify', verifyToken);

// Suppliers
apiRoutes.post('/suppliers', suppliers.createSupplier);
apiRoutes.get('/suppliers', suppliers.getSuppliersByCompany);
apiRoutes.get('/suppliers/length', suppliers.getSuppliersLength);
apiRoutes.get('/suppliers/find', suppliers.getSuppliersBySearch);
apiRoutes.get('/suppliers/names', suppliers.getSuppliersNames);
app.get('/suppliers/:id', suppliers.getSuppliersById);

// Upload Images
apiRoutes.post('/image', uploadImage);

// Incomes and Expenses
apiRoutes.post('/records', incomesAndExpenses.createNewRecord);
apiRoutes.get('/records', incomesAndExpenses.getTotalIncomesAndExpenses);
apiRoutes.get('/records/incomes', incomesAndExpenses.getRecordsByIncomes);
apiRoutes.get('/records/expenses', incomesAndExpenses.getRecordsByExpenses);
app.get('/record', incomesAndExpenses.getRecordById);



app.listen(port, () => {
    console.log(`running on port ${port}`)
});


