const jwt = require('jsonwebtoken');

const { verifyJWT } = require('../methods/token');

const verifyToken = (req, res) => {
  verifyJWT(req.query.token)
  .then(response => {
    if (response) {
      res.json({ success: true, companyName: response.companyName });
    }
  })
  .catch((err) => {
    err['success'] = false;
    res.json(err)
  })
};

module.exports = { verifyToken };