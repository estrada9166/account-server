const companies = require('../database/companies');
const { createJWT } = require('../methods/token');
const bcrypt = require('bcrypt');

const saltRounds = 10;

const createCompany = (req, res) => {
  const companyName = req.body.companyName;
  const email = req.body.email;
  const password = req.body.password;  
  
  bcrypt.hash(password, saltRounds)
  .then(hash => {
    return companies.createCompanyOnDb(companyName, email, hash);
  })
  .then(data => {
    const token = createJWT(data.token)
    res.json({ companyName, token, success: true, 
      message: 'company created on the db' });
  })
  .catch(err => {
    console.log(err);
  })
};

const signIn = (req, res) => {
  const email = req.body.email;
  const password = req.body.password;  

  companies.getCompanyByEmail(email)
  .then(company => {
    const userInfo = company[0];
    return bcrypt.compare(password, userInfo.password)
  }) 
  .then(response => {
    if(response) {;
      return companies.updateCompanyToken(email);
    } else {
      res.json({ success: false })
    }
  })
  .then((company) => {
    const token = createJWT(company.token)
    res.json({ token, success: true, companyName: company.name })
  })
  .catch(err => {
    res.json(err)
  })
}

module.exports = { createCompany, signIn };
