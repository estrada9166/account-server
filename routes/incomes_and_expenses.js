const incomes_and_expenses = require('../database/incomes_and_expenses');

const createNewRecord = (req, res) => {
  const recordInfo = JSON.parse(req.body.data);

  incomes_and_expenses.createNewRecord(recordInfo, res.locals.companyId)
  .then(() => {
    res.json({ success: true })
  })
  .catch((err) => {
    console.log(err)
  })
}

const getTotalIncomesAndExpenses = (req, res) => {
  incomes_and_expenses.getTotalIncomesAndExpenses(res.locals.companyId)
  .then(incomesAndExpenses => {
    console.log(incomesAndExpenses)
    res.json({ success: true, incomesAndExpenses})
  })
  .catch(err => {
    console.log(err)
  })
}

const getRecordsByIncomes = (req, res) => {
  incomes_and_expenses.getRecordsByType('income', res.locals.companyId)
  .then(response => {
    res.json({ success: true, incomes: response})
  })
  .catch(err => {
    res.json(err);
  })
}

const getRecordsByExpenses = (req, res) => {
  incomes_and_expenses.getRecordsByType('expense', res.locals.companyId)
  .then(response => {
    res.json({ success: true, expenses: response})
  })
  .catch(err => {
    res.json(err);
  })
}

const getRecordById = (req, res) => {
  incomes_and_expenses.getRecordById(req.query.id)
  .then(response => {
    res.json({ success: true, record: response})
  })
  .catch(err => {
    res.json(err)
  })
}

module.exports = { 
                  createNewRecord, 
                  getTotalIncomesAndExpenses, 
                  getRecordsByIncomes,
                  getRecordsByExpenses,
                  getRecordById
                 }; 