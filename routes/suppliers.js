const suppliers = require('../database/suppliers');

const createSupplier = (req, res) => {
  const supplierInfo = JSON.parse(req.body.supplierInfo);

  suppliers.createNewSupplier(supplierInfo, res.locals.companyId)
  .then(() => {
    res.json({ success: true })
  })
  .catch((err) => {
    console.log(err);
  })
}

const getSuppliersLength = (req, res) => {
  suppliers.getCompanySuppliersLengthById(res.locals.companyId)
  .then(data => {
    res.json({ success: true, length: data})
  })
}

const getSuppliersByCompany = (req, res) => {
  suppliers.getCompanySuppliersById(res.locals.companyId, req.query.from)
  .then(data => {
    res.json({ success: true, suppliers: data })
  })
  .catch(err => {
    res.json(err)
  })
}

const getSuppliersById = (req, res) => {
  suppliers.getSuppliersById(req.params.id)
  .then(data => {
    res.json({ success: true, suppliers: data })
  })
  .catch(err => {
    res.json(err)
  })
}

const getSuppliersBySearch = (req, res) => {
  suppliers.getSupplierByKeyWord(res.locals.companyId, req.query.text)
  .then(data => {
    res.json({ success: true, suppliers: data })
  })
  .catch(err => {
    res.json(err)
  })
}

const getSuppliersNames = (req, res) => {
  suppliers.getCompanySuppliersNamesById(res.locals.companyId)
  .then(data => {
    res.json({ success: true, suppliers: data })
  })
  .catch(err => {
    res.json(err)
  })
}

module.exports = { createSupplier, 
                   getSuppliersLength,
                   getSuppliersByCompany, 
                   getSuppliersById,
                   getSuppliersBySearch,
                   getSuppliersNames
                 };
