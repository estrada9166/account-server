const db  = require('./db');


const createNewRecord = (recordInfo, id) => {
  return db.one(`INSERT INTO incomes_and_expenses(
                                                  type,
                                                  concept,
                                                  amount,
                                                  tax,
                                                  retentions,
                                                  discount,
                                                  total_amount,
                                                  date,
                                                  file_url,
                                                  extra_info,
                                                  supplier_id,
                                                  company_id
  ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)`, 
  [
    recordInfo.type ? recordInfo.type : 'income',
    recordInfo.concept ? recordInfo.concept : '',
    recordInfo.amount ? recordInfo.amount : 0,
    recordInfo.tax ? recordInfo.tax : 0,
    recordInfo.retentions ? recordInfo.retentions : 0,
    recordInfo.discount ? recordInfo.discount : 0,
    recordInfo.totalAmount,
    recordInfo.date ? recordInfo.date : null,
    recordInfo.fileUrl ? recordInfo.fileUrl : '',
    recordInfo.extraInfo ? recordInfo.extraInfo : '',
    recordInfo.supplierId,
    id
  ])
}

const getTotalIncomesAndExpenses = id => {
  return db.any(`SELECT type, SUM(total_amount) FROM incomes_and_expenses WHERE 
    company_id = $1 GROUP BY type`, [id])
}

const getRecordsByType = (type, id) => {
  return db.any(`SELECT incomes_and_expenses.*, suppliers.name AS supplier_name, 
    TO_CHAR(incomes_and_expenses.date, 'DD MON') AS date_format 
    FROM incomes_and_expenses INNER JOIN suppliers ON 
    incomes_and_expenses.supplier_id = suppliers.id WHERE incomes_and_expenses.type = $1  
    AND incomes_and_expenses.company_id = $2 ORDER BY incomes_and_expenses.date`, [type, id])
}

const getRecordById = id => {
  return db.one(`SELECT *, TO_CHAR(date, 'DD MON YY') 
    AS date_format FROM incomes_and_expenses WHERE id = $1`, [id]);
}

module.exports = { 
                  createNewRecord, 
                  getTotalIncomesAndExpenses, 
                  getRecordsByType,
                  getRecordById
                }