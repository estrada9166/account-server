const db  = require('./db');
const { randomString } = require('../methods/random');

db.connect();

const createCompanyOnDb = (companyName, email, hash) => {
  const token = randomString(32, '#aA');
  return db.one(`INSERT INTO companies(name, email, password, token) 
    VALUES($1, $2, $3, $4) RETURNING token`, [companyName, email, hash, token]);
}

const updateCompanyToken = email => {
  const token = randomString(32, '#aA');  
  return  db.one(`UPDATE companies SET token = $1  WHERE EMAIL = $2 
    RETURNING token, name`, [token, email]);
}

const getCompanyByEmail = email => {
  return db.any(`SELECT ID, NAME, PASSWORD, TOKEN FROM companies 
    WHERE EMAIL = $1`, [email]);
}

const getCompanyIdByToken = token => {
  return db.one('SELECT ID FROM companies WHERE TOKEN = $1', [token]);
}

module.exports = { 
  createCompanyOnDb, 
  updateCompanyToken, 
  getCompanyByEmail,
  getCompanyIdByToken
 }