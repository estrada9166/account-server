// ID                     PK INTEGER
// NAME                   TEXT
// CONTACT_NAME           TEXT
// CONTACT_EMAIL          TEXT
// CONTACT_TELEPHONE      INTEGER
// SUPPLIER_ID_URL        TEXT
// TYPE                   TEXT
// EXTRA_INFO             TEXT
// CONTRACTOR_ID          INTEGER
// SUPPLIER_ID            TEXT
// TRADE_NAME             TEXT
// BANK_NAME              TEXT
// ACCOUNT_TYPE           TEXT
// ACCOUNT_NUMBER         TEXT


const db  = require('./db');

const createNewSupplier = (supplierInfo, id) => {
  return db.one(`INSERT INTO suppliers(
                                        name, 
                                        trade_name,
                                        identification,
                                        id_file_url,
                                        contact_name,
                                        contact_email,
                                        contact_telephone,
                                        type,
                                        bank_name,
                                        bank_account_type,
                                        bank_account_number,
                                        extra_info,
                                        contractor_id
    ) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13
                                      ) RETURNING name`,  
    [
      supplierInfo.supplierName, 
      supplierInfo.companyTradeName,
      supplierInfo.supplierId,
      supplierInfo.supplierIdUrl,
      supplierInfo.contactName,
      supplierInfo.contactEmail, 
      supplierInfo.contactTelephone, 
      supplierInfo.supplierType,
      supplierInfo.supplierBankName,
      supplierInfo.supplierAccountType,
      supplierInfo.supplierBankAccount,  
      supplierInfo.extraInfo,
      id,
    ])
}

const getCompanySuppliersLengthById = id => {
  return db.one(`SELECT COUNT(id) FROM suppliers WHERE contractor_id = $1`, [id])
}

const getCompanySuppliersById = (id, from) => {
  return db.any(`SELECT id, name, 
                            contact_name,   
                            contact_email, 
                            contact_telephone 
              FROM suppliers WHERE contractor_id = $1 ORDER BY name ASC LIMIT 16 
              OFFSET $2`,
              [id, from])
}

const getSuppliersById = id => {
  return db.one(`SELECT id, name, trade_name, identification, id_file_url,
                 contact_name, contact_email, contact_telephone, type,  
                 bank_name, bank_account_type, bank_account_number, extra_info
                  FROM suppliers WHERE id = $1`, [id])
}

const getSupplierByKeyWord = (id, text) => {
  return db.any(`SELECT id, name, contact_name, contact_email, contact_telephone 
                FROM suppliers WHERE contractor_id = $1 AND name ILIKE '%$2#%' OR
                contact_name ILIKE '%$2#%' OR contact_email ILIKE '%$2#%' 
                LIMIT 16`, [id, text])
}

const getCompanySuppliersNamesById = id => {
  return db.any(`SELECT id, name FROM suppliers WHERE contractor_id = $1 
    ORDER BY name ASC`, [id])
}

module.exports = { createNewSupplier,
                   getCompanySuppliersLengthById,
                   getCompanySuppliersById, 
                   getSuppliersById,
                   getSupplierByKeyWord,
                   getCompanySuppliersNamesById
                  }