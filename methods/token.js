const jwt = require('jsonwebtoken');

const createJWT = token => {
  return jwt.sign({
      userInfo: token
  }, 'secret account key');
}

const verifyJWT = token => {
  return new Promise(resolve => {
    resolve(jwt.verify(token, 'secret account key'));
  })
}

module.exports = { createJWT, verifyJWT };